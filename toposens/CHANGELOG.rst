^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package toposens
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.1.0 (2019-06-21)
------------------
* Added support for ROS Kinetic on Ubuntu 16.04
* Enhanced setup guide for all packages
* Added urdf for turtlebot with TS3
* Added sync integration for multiple device operation
* Contributors: Adi Singh, Sebastian Dengler, Christopher Lang

1.0.0 (2019-05-14)
------------------

0.9.6 (2019-05-13)
------------------

0.9.5 (2019-05-03)
------------------

0.9.3 (2019-04-26)
------------------

0.9.2 (2019-04-17)
------------------
* Fixed package versions
* Contributors: Adi Singh

0.9.0 (2019-04-09)
------------------

0.8.1 (2019-03-29)
------------------

0.8.0 (2019-03-08)
------------------
* Created Command class with overloaded constructors
* Contributors: Christopher Lang

0.7.0 (2019-02-27)
------------------
* Added native PCL integration
* Implemented markers using rviz visual tools
* Refactored to standard ROS package template
* Contributors: Adi Singh

0.5.0 (2019-02-07)
------------------
