/** @file     serial.h
 *  @author   Adi Singh, Sebastian Dengler
 *  @date     January 2019
 */

#ifndef SERIAL_H
#define SERIAL_H

#include <termios.h>
#include <sstream>
#include <toposens_driver/command.h>


namespace toposens_driver
{
/** @brief  Provides low-level I/O access to TS data stream using
 *  native Unix API.
 *  @details  Sets up a UART bridge to access raw data packets
 *  from a TS device. Maintains simple read-write access at the
 *  given baud rate. Methods defined here are independent of ROS.
 */
class Serial
{
  public:
    /** Opens a persistent serial connection at the given port.
    *  @param port Device endpoint usually of the form /dev/ttyUSB*.
    */
    Serial(std::string port);
    ~Serial();

    /** Extracts a single TS data frame into the given iostream.
      * @param data Points to a string stream expecting a sensor frame.
      */
    void getFrame(std::stringstream &data);

    /** Writes the given bytes to the serial stream and confirms a parameter update.
      * Usually used for updating sensor settings during runtime. 
      * @param cmd Instance of command class specifying a parameter to be updated and the desired value.
      * @returns True if sensor handshake is received to confirm settings update, false otherwise.
      */
    bool sendCmd(Command cmd);

  private:
    int _fd;    /**< Linux file descriptor pointing to TS device port.*/
    std::string _port;  /**< Stored device port for future access.*/
    const unsigned int kBaud = B921600; /**< Baud rate needed for TS device comms.*/

    /**
      * Blocks execution of driver until an acknowledgement message is received or watchdog timer expires.
      * A valid acknowledgement will confirm the send parameter settings command, e.g. the command "CsPuls00003"
      * should result in following valid acknowledgement "S000004C00003E".
      *
      *  An acknowledgement frame corresponds to a single parameter update and has the following format:
      *  @n - Starts with char 'S'
      *  @n - 6 bytes of bit shifts to decode parameter level as defined in Command::Parameters,
      *   '-1' denotes unknown parameter
      *  @n - Char 'C', indicates a command acknowledgement frame
      *  @n - 5 bytes of firmware parameter value
      *  @n - Ends with char 'E'
      *
      * @param cmd instance of parameter update command
      * @return true if acknowledgement verifies parameter update, false otherwise
      */
    bool _waitForAcknowledgement(Command cmd);

};

} // namespace toposens_driver

#endif // SERIAL_H