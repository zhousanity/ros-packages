/**  @file     command.h
  *  @author   Adi Singh, Christopher Lang
  *  @date     March 2019
  */

#ifndef COMMANDS_H
#define COMMANDS_H

#include <toposens_driver/TsDriverConfig.h>
#define USE_INTERNAL_TEMPERATURE -100.0

namespace toposens_driver
{

/** Defined in TS firmware and exposed as public constants to
 *  be accessed by the Command API. Ranges and default values
 *  are stated in the package's cfg file.
 *  The enumeration values are used as dynamic reconfiguration levels by the dynamic reconfiguration server, which
 *  matches parameters based their level bit mask. In case of multiple parameter updates, the bit masks are
 *  concatenated by an OR operation.
 */
enum TsParam
{  
  UseExternalTemperature  = 0b00000001, /**< Switch between internal temperature sensor value or user-defined temp value. */
  EchoRejectionThreshold  = 0b00000010, /**< Minimum amplitude for an echo to be considered valid [0 to 20]. */
  NoiseIndicatorThreshold = 0b00000100, /**< Normalized noise level on ADC signals to mark processed points as noisy. */
  NumberOfPulses          = 0b00001000, /**< Number of ultrasonic pulses emitted in every transmission cycle [0 to 20]. */
  PeakDetectionWindow     = 0b00010000, /**< Kernel size applied on ADC signals for peak detection [1 to 100]. */
  ExternalTemperature     = 0b00100000, /**< Temperature value used to calibrate speed-of-sound. */
  ScanMode                = 0b01000000, /**< Scan Mode of Sensor: 0 for scanning continuously; 1 for scanning once; 2 for listening only (once) */
};

/** @brief Generates firmware-compatible commands for tuning
 *  performance parameters.
 *
 *  @details Firmware defines commands in two formats, singular
 *  and dimensional. Singular commands expect one parameter value,
 *  dimensional commands expect XYZ limits defining a spatial cuboid.
 *  Currently, only the voxel filtering command is dimensionally formatted.
 *
 *  Note that all desired values are transmitted as zero-padded 5-byte
 *  strings, and the first byte is always reserved for the arithmetic
 *  sign: 0 for positive values, - for negative values.
 *
 *  Note that modifications on these parameters are done on the sensor chip, not in this driver
 */
class Command
{
  public:
    /** Builds a command message accepted by the TS firmware.
     *  @param param Setting name from the enumerated command list.
     *  @param value Desired integer value for sensor parameter.
     */
    Command(TsParam param, float value);

    /**
     * Returns this object's parameter enumeration value denoting the firmware parameter to be updated.
     * @return parameter
     */
    TsParam getParam(){ return _param; }

    /** Returns the latest command message produced by generate().
     *  @returns Pointer to a char array containing command.
     */
    char* getBytes() { return _bytes; }

    /** Looks up command name defined by the TsDriver configuration to
     *  given setting parameters.
     *  @returns Corresponding dynamic reconfigure name.
     */
    std::string getParamName();


  private:
    const int MAX_VALUE =  9999;
    const int MIN_VALUE = -9999;

    char _bytes[50];   /**< Large enough buffer to hold a well-formed command.*/
    static const char kPrefix = 'C'; /**< Designates a string as a firmware command.*/
    TsParam _param; /**< Parameter to be encoded in command bytes.*/

    /** Looks up command keys defined by the TS firmware corresponding to
     *  given setting parameters.
     *  @param param Setting name from the enumerated command list.
     *  @returns Corresponding firmware parameter key.
     */
    std::string _getKey(TsParam param);

};

} // namespace toposens_driver

#endif
