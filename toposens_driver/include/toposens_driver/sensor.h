/** @file     sensor.h
 *  @author   Adi Singh, Sebastian Dengler
 *  @date     January 2019
 */

#ifndef SENSOR_H
#define SENSOR_H

#include <ros/ros.h>
#include <dynamic_reconfigure/server.h>

#include <toposens_driver/serial.h>
#include <toposens_msgs/TsScan.h>
#include <toposens_driver/TsDriverConfig.h>

namespace toposens_driver
{
/** ROS topic for publishing TsScan messages. */
static const char kScansTopic[] = "ts_scans";
/** Maximum number of messages held in buffer for #kScansTopic. */
static const int kQueueSize = 100;

/** @brief  Converts raw sensor data to ROS friendly message structures.
 *  @details  Parses a TsScan from a single input data frame by extracting
 *  its header information and the vector of TsPoints contained in its payload.
 *  A TsScan contains timestamped header information followed by a vector
 *  of TsPoints. A single TsPoint has a 3D location (x, y, z) and an
 *  associated intensity. Messages are published to topic #kScansTopic.
 */
class Sensor
{
  public:
    /** The three different scan modes of a single sensor that are send via a command:
     *  ScanContinuously will make the sensor scan until told otherwise,
     *  ScanOnce will make the sensor scan for a single frame,
     *  ListenOnce will make the sensor listen to the echos for a single frame
     *  without sending an ultrasonic pulse first.
     */
     enum ScanMode
     {
       ScanContinuously,
       ScanOnce,
       ListenOnce
     };

    /** Initiates a serial connection and transmits default settings to sensor.
     *  @param nh Public nodehandle for pub-sub ops on ROS topics.
     *  @param private_nh Private nodehandle for accessing launch parameters.
     */
    Sensor(ros::NodeHandle nh, ros::NodeHandle private_nh);

    /** Is used to initialize a specific sensor when multiple sensors are used.
     *  Needs to be passed an individual private nodehandle for setting up the
     *  dynamic reconfigure server for the sensor parameters.
     *  @param nh Public nodehandle for pub-sub ops on ROS topics.
     *  @param private_nh Private nodehandle for setting up the dynamic reconfigure.
     *  @param port Serial port that the specific sensor is connected to.
     *  @param frame ID of the coordinate frame the specific sensor publishes to.
     */
    Sensor(ros::NodeHandle nh, ros::NodeHandle private_nh, std::string port, std::string frame_id);

    ~Sensor() {}

    /** Sends a specific scan command to the sensor.
     *  @param the scan command that is sent to the sensor.
     */
    void setMode(ScanMode scan_mode);

    /** Retrieves raw sensor data frames and publishes TsScans extracted from them.
     *  @returns True if scan contains any valid data points. False for an empty scan.
     */
    bool poll(void);

    /** Shuts down serial connection to the sensor. */
    void shutdown(void);


  private:
    /** Structure generated from cfg file for storing local copy of sensor parameters.*/
    typedef dynamic_reconfigure::Server<TsDriverConfig> Cfg;

    /** Transmits settings commands on startup with initial data
     *  from the config server.
     */
    void _init(void);

    /** Callback triggered when a parameter is altered on the dynamic
     *  reconfigure server.
     *  Determines which setting has changed and transmits the associated
     *  (well-formed) settings command to the serial stream.
     *  @param cfg Structure holding updated values of all parameters on server.
     *  @param level Indicates parameter that triggered the callback.
     */
    void _reconfig(TsDriverConfig &cfg, uint32_t level);

    /** Extracts TsPoints from the current data frame and reads them
     *  into the referenced TsScan object.
     *  @param frame is the data frame received.
     */
    void _parse(const std::string &frame);

    /** Efficiently converts a char array representing a signed integer to
     *  its numerical value.
     *  @param i String iterator representing an integer value.
     *  @returns Signed float value after conversion.
     *  @throws std::bad_cast String contains non-numerical characters.
     */
    float _toNum(auto &i);

    std::string _frame_id;     /**< Frame ID assigned to TsScan messages.*/
    TsDriverConfig _cfg;    /**< Maintains current values of all config params.*/
    std::unique_ptr<Cfg> _srv;  /**< Pointer to config server*/

	  ros::Publisher _pub;    /**< Handler for publishing TsScans.*/
    std::unique_ptr<Serial> _serial;  /**< Pointer for accessing serial functions.*/
    std::stringstream _buffer;  /**< Buffer for storing a raw data frame.*/

    toposens_msgs::TsScan _scan;

};

} // namespace toposens_driver

#endif
