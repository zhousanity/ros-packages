/** @file     serial.cpp
 *  @author   Adi Singh, Sebastian Dengler
 *  @date     January 2019
 */

#include "toposens_driver/serial.h"

#include <fcntl.h>
#include <ros/console.h>
#include <string.h>

namespace toposens_driver
{
/** Various termios struct flags are optimized for a connection
 *  that works best with the TS firmware. Any intrinsic flow
 *  control or bit processing is disabled.
 *
 *  Connection is a non-blocking thread that returns when at
 *  least 1 byte is received or 0.1s has passed since the last
 *  read operation.
 */
Serial::Serial(std::string port)
{
  _fd = -1;
  _port = port;

	// Open serial port to access sensor stream
	_fd = open(_port.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);

	int retry_counter = 5;

  while (_fd == -1)
  {
    std::string msg = "Error opening connection at" + _port + ": " + strerror(errno);

    if(retry_counter--)
    {
      ROS_WARN_STREAM(msg);
      ros::Duration(0.5).sleep();

      ROS_INFO_STREAM("Retrying to establish connection at " << _port << " ...");
      _fd = open(_port.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);
    }
    else
    {
      throw std::runtime_error(msg );
    }
  }
  ROS_DEBUG("Toposens serial established with fd %d\n", _fd);

	// Set options of serial data transfer
	struct termios tty;
  memset(&tty, 0, sizeof(tty));

  // Get current attributes of termios structure
  if(tcgetattr(_fd, &tty) != 0)
  {
    ROS_WARN("Error retrieving attributes at %s: %s", _port.c_str(), strerror (errno));
    return;
  }

  // set I/O baud rate
  cfsetispeed(&tty, kBaud);
  cfsetospeed(&tty, kBaud);

  // enable reading, ignore ctrl lines, 8 bits per byte
  tty.c_cflag |= CREAD | CLOCAL | CS8;
  // turn off parity, use one stop bit, disable HW flow control
  tty.c_cflag &= ~(PARENB | CSTOPB | CRTSCTS);

  // disable canonical mode and echoes
  tty.c_lflag &= ~(ICANON | ECHO | ECHOE | ECHONL);

  // disable software flow control
  tty.c_iflag &= ~(IXON | IXOFF | IXANY);
  // disable break condition handling
  tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK);
  // disable special data pre-processing
  tty.c_iflag &= ~(ISTRIP | INLCR | IGNCR | ICRNL);

  // disable special data post-processing
  tty.c_oflag &= ~(OPOST | ONLCR);

  // wait for at least 1 byte to be received
  // length of a valid empty data frame
  tty.c_cc[VMIN] = 1;
  // wait for 0.1s till data received
  tty.c_cc[VTIME] = 1;


  // Set attributes of termios structure
  if(tcsetattr(_fd, TCSANOW, &tty) != 0)
  {
    ROS_WARN("Error configuring device at %s: %s", _port.c_str(), strerror (errno));
    return;
  }
  
  ROS_DEBUG("Serial settings updated:\n  BaudRate = %d \n  DataBits = 8 \n  Parity = disabled", kBaud);
  tcflush(_fd, TCIFLUSH);			// Discard old data in RX buffer
  ROS_INFO("Device at %s ready for communication", _port.c_str());
}

/** Flushes out bits from the serial pipe and closes its
 *  corresponding linux file descriptor.
 */
Serial::~Serial(void)
{
  ROS_INFO("Closing serial connection...");
  tcflush(_fd, TCIOFLUSH);

  if (close(_fd) == -1)
  {
		ROS_ERROR("Error closing serial connection: %s", strerror(errno));
	}
  else 
  {
    ROS_INFO("Serial connection killed");
  }
}
	

/** Reads incoming bytes to the string stream pointer till
 *  the firmware-defined frame terminator 'E' is reached.
 *  Returns if no data has been received for 1 second.
 */
void Serial::getFrame(std::stringstream &data)
{
  char buffer[1];
	int nBytes = 0;
  ros::Time latest = ros::Time::now();

  do
  {
    memset(&buffer, '\0', sizeof(buffer));
    nBytes = read(_fd, &buffer, sizeof(buffer));

    if (nBytes < 1)
    {
      ros::Duration(0.01).sleep();
      continue;
    }

    data << buffer;
    latest = ros::Time::now();

    if (buffer[nBytes-1] == 'E') break;

  } while (ros::Time::now() - latest < ros::Duration(1));
}


/** Note that this returns true as long as data is written to
 *  the serial stream without error. A success handshake from
 *  the sensor confirming the settings update is not currently
 *  checked for.
 */
bool Serial::sendCmd(Command cmd)
{
  char* bytes = cmd.getBytes();

  try {

    if (_fd == -1) {
      std::string msg = "Connection at " + _port + " unavailable: " + strerror(errno);
      throw std::runtime_error(msg);
    }

    int tx_length = write(_fd, bytes, strlen(bytes));

    if (tx_length != -1) {
      ROS_DEBUG("Bytes transmitted: %s", bytes);
      return _waitForAcknowledgement(cmd);
    } else {
      ROS_ERROR("Failed to transmit %s: %s", bytes, strerror(errno));
      return false;
    }
  }
  catch (const std::exception& e)
  {
    ROS_ERROR("%s: %s", e.what(), cmd.getBytes());
    return false;
  }

}

/**
 * Blocks execution of driver until an acknowledgement message is received or watchdog timer expires.
 */
bool Serial::_waitForAcknowledgement(Command cmd)
{
  std::stringstream buffer;
  std::string data;
  size_t frame_start = -1;

  ros::Time latest = ros::Time::now();

  ROS_DEBUG("Awaiting acknowledgement for %s ...", cmd.getBytes());

  do
  {
    buffer.str(std::string());
    buffer.clear();

    this->getFrame(buffer);

    data = buffer.str().c_str();
    frame_start = data.find('S');

    if(frame_start < 0) continue;

    if(ros::Time::now() - latest > ros::Duration(5)){
      ROS_WARN_STREAM("Settings update timed out! - Aborting.");
      return false;
    }

  } while(data[frame_start + 7] != 'C');

  // Evaluate received acknowledgement.
  int ack = (data[frame_start + 6] - '0');
  if (data[frame_start + 5] == '-') ack *= -1;

  // Compare parameter levels of command and acknowledgement frames.
  if((int)cmd.getParam() == (1 << ack))
  {

    float val = std::atof(&data[frame_start + 8]);
    if(cmd.getParam() == TsParam::ExternalTemperature) val = val/10.0;

    // Compare 5 value bytes of command and acknowledgement frames.
    if (strncmp(&data[frame_start + 8], &cmd.getBytes()[6], 5) == 0)
    {
      if (cmd.getParam() != TsParam::ScanMode)
      {
        ROS_INFO_STREAM("TS parameter: " << cmd.getParamName() << " updated to " << val);
      }
      return true;
    }
    else if(strcmp(cmd.getBytes(), "CsTemp-1000\r") == 0)
    {
      ROS_INFO_STREAM("TS parameter: " << cmd.getParamName() << " set to use internal temperature sensor.");
      return true;
    }
    else
    {
      ROS_WARN_STREAM("TS parameter: " << cmd.getParamName() << " clipped to " << val );
    }
  }
  else
  {
    ROS_WARN_STREAM("TS parameter: " << cmd.getParamName() << " value update failed!");
  }

  return false;
}

} // namespace toposens_driver
