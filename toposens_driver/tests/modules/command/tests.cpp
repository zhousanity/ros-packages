/** @file     command_test.cpp
 *  @author   Christopher Lang, Roua Mokchah, Adi Singh
 *  @date     March 2019
 */

#include <gtest/gtest.h>
#include <toposens_driver/command.h>

using namespace toposens_driver;

class CommandTest : public ::testing::Test
{
public:
  const std::string TAG = "\033[36m[DriverCommandTest]\033[00m - ";

protected:
  void SetUp() {}

  void TearDown() {}

  void matchCmd(const char* msg, TsParam p, int val, const char* exp)
  {
    std::cerr << TAG << "\t" << msg << "...";

    Command cmd(p, val);
    EXPECT_STREQ(cmd.getBytes(), exp);

    std::cerr << "done\n";
  }
};


/** @test Checks if command messages are correctly generated using valid paramenter values. */
TEST_F(CommandTest, validValues)
{
  std::cerr << TAG << "<validValues>\n";

  this->matchCmd("Number of pulses with 5",           TsParam::NumberOfPulses,           5,   "CsPuls00005\r");
  this->matchCmd("Peak detection window with 42",     TsParam::PeakDetectionWindow,      42,  "CsPeak00042\r");
  this->matchCmd("Echo rejection threshold with 15",  TsParam::EchoRejectionThreshold,   15,  "CsReje00015\r");
  this->matchCmd("Noise indicator threshold 100",     TsParam::NoiseIndicatorThreshold,  100, "CsNois00100\r");
  this->matchCmd("Calibration temp with -3",          TsParam::ExternalTemperature,      -3,  "CsTemp-0030\r");

  std::cerr << TAG << "</validValues>\n";
}

/** @test Attempts to generate command messages with both invalid parameter values.
 *  Clips value to the closest limit if input is out of range.
 */
TEST_F(CommandTest, outOfRangeValues)
{
  std::cerr << TAG << "<outOfRangeValues>\n";

  this->matchCmd("Number of pulses with -4678",           TsParam::NumberOfPulses,           -4678,   "CsPuls-4678\r");
  this->matchCmd("Peak detection window with NULL",       TsParam::PeakDetectionWindow,      NULL,    "CsPeak00000\r");
  this->matchCmd("Echo rejection threshold with 00000",   TsParam::EchoRejectionThreshold,   000000,  "CsReje00000\r");
  this->matchCmd("Noise indicator threshold with 00604",  TsParam::NoiseIndicatorThreshold,  00604,   "CsNois00388\r");
  this->matchCmd("External temperature with INT_MAX",     TsParam::ExternalTemperature,      INT_MAX, "CsTemp09999\r");

  std::cerr << TAG << "</outOfRangeValues>\n";
}


int main(int argc, char **argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "ts_driver_command_test");
  ros::NodeHandle nh;
  return RUN_ALL_TESTS();
}
