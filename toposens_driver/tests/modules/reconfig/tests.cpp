/** @file     reconfig_test.cpp
 *  @author   Adi Singh, Christopher Lang, Roua Mokchah
 *  @date     April 2019
 */

#include <fcntl.h>
#include <ros/ros.h>
#include <gtest/gtest.h>
#include <boost/thread.hpp>
#include <dynamic_reconfigure/server.h>


char kReconfigTestBuff[100] = "";

class ReconfigTest : public ::testing::Test
{
public:
  const std::string TAG = "\033[36m[DriverReconfigTest]\033[00m - ";

protected:
  ros::NodeHandle* private_nh;
  std::string mock_sensor;
  int conn_handle;
  char buff[100];
  boost::shared_ptr<boost::thread> ackThread;

  void SetUp()
  {
    memset(&buff, '\0', sizeof(buff));
    private_nh = new ros::NodeHandle("~");
    private_nh->getParam("mock_sensor", mock_sensor);
    conn_handle = open(mock_sensor.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);

    ackThread = std::make_unique<boost::thread>(sendCommandAcknowledgements);
    ros::Duration(1).sleep(); // Wait for complete initialization of ts_driver_node

    // Reads in initial commands sent by Sensor::_init
    int n_bytes = 0;
    while(!strlen(kReconfigTestBuff))
    {
      n_bytes = read(conn_handle, &kReconfigTestBuff, sizeof(kReconfigTestBuff));
      if (n_bytes > 0) break;
      ros::Duration(0.01).sleep();
      ros::spinOnce();
    }

    ackThread->interrupt();
    ackThread->join();

  }

  void TearDown()
  {
    close(conn_handle);
    delete private_nh;
  }

  /** Updates the reconfig server with a given parameter and value.
   *  Uses the dynamic_reconfigure library to dynamically update ts_driver_node parameters.
   */
  void updateCfg(std::string param_name, int param_value)
  {
    std::cerr << TAG << "\tUpdating parameter server with "
      << param_name << " of " << param_value << "...";

    dynamic_reconfigure::IntParameter int_param;
    int_param.name = param_name;
    int_param.value = param_value;

    dynamic_reconfigure::Config conf;
    conf.ints.push_back(int_param);

    dynamic_reconfigure::ReconfigureRequest req;
    req.config = conf;

    ackThread = std::make_unique<boost::thread>(sendCommandAcknowledgements);

    dynamic_reconfigure::ReconfigureResponse res;
    ros::service::call("/ts_driver_node/set_parameters", req, res);
    ros::Duration(0.1).sleep();

    ackThread->interrupt();
    ackThread->join();

    ros::spinOnce();

    std::cerr << "done\n";
  }

  /** Listens for commands on the mock sensor. */
  void listen()
  {
    std::cerr << TAG << "\tListening for commands on " << mock_sensor << "...";

    int n_bytes = 0;
    while(n_bytes < 1)
    {
      n_bytes = read(conn_handle, &buff, sizeof(buff));
      ros::Duration(0.01).sleep();
    }
    std::cerr << "done\n";
  }


  /** Sends artificial parameter update acknowledgements to unblock sensor constructor. */
  static void sendCommandAcknowledgements(){
    std::string mock_sensor;
    ros::NodeHandle* private_nh;
    private_nh = new ros::NodeHandle("~");
    private_nh->getParam("mock_sensor", mock_sensor);

    std::string acknowledgement("S000004C00003E");

    int fd = open(mock_sensor.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);

    while(true)
    {
      try
      {
        boost::this_thread::sleep(boost::posix_time::milliseconds(100));
        write(fd,
              acknowledgement.c_str(),
              acknowledgement.size()
        );
      }
      catch(boost::thread_interrupted&)
      {
        delete private_nh;
        return;
      }
    }
  }

};

/** @test Compares expected initialization command-string with that sent by Sensor::_init.
 *  Message should be well-formed and output clipped to paramter value limits.
 */
TEST_F(ReconfigTest, checkInitConfig)
{
  std::cerr << TAG << "<checkInitConfig>\n";

  // @todo Make this dynamic according to the initial parameters that are set in the .cfg file.
  std::string exp = "CsPuls00005\rCsPeak00003\rCsReje00005\rCsNois02000\rCsTemp-1000\rCsMode00000\r";
  std::cerr << TAG << "\tChecking initial command flush...";
  EXPECT_STREQ(kReconfigTestBuff, exp.c_str());
  std::cerr << "done\n";

  std::cerr << TAG << "</checkInitConfig>\n";
}

/** @test Updates a sensor parameter and confirms that the corresponding well-formed
 *  command string is transmitted to the mock sensor.
 */
TEST_F(ReconfigTest, changeNumPulse)
{
  std::cerr << TAG << "<changeNumPulse>\n";

  this->updateCfg("num_pulses", 12);
  this->listen();
  EXPECT_EQ(std::string(buff),"CsPuls00012\r");

  std::cerr << TAG << "</changeNumPulse>\n";
}

int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "ts_driver_reconfig_test");
  ros::NodeHandle nh;
  return RUN_ALL_TESTS();
}
