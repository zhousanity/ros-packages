/** @file     sensor_test.cpp
 *  @author   Adi Singh, Christopher Lang, Roua Mokchah
 *  @date     March 2019
 */

#include <fcntl.h>
#include <fstream>
#include <gtest/gtest.h>
#include <boost/thread.hpp>

#include <toposens_driver/sensor.h>

using namespace toposens_driver;

class SensorTest : public ::testing::Test
{
public:
  const std::string TAG = "\033[36m[DriverSensorTest]\033[00m - ";

protected:
  Sensor* dev;
  toposens_msgs::TsScan scan;
  ros::Subscriber sub;
  ros::NodeHandle* private_nh;
  std::string mock_sensor, driver_port, curr_frame;

  void SetUp()
  {
    ros::NodeHandle nh;
    private_nh = new ros::NodeHandle("~");
    private_nh->getParam("mock_sensor", mock_sensor);
    private_nh->getParam("port", driver_port);
    sub = nh.subscribe(kScansTopic, kQueueSize, &SensorTest::store, this);


    boost::thread ackThread(sendCommandAcknowledgements);
    dev = new Sensor(nh, *private_nh);
    ackThread.interrupt();
    ackThread.join();

    scan.header.stamp = ros::Time::now();
    scan.header.frame_id = "toposens";
    scan.points.clear();
  }

  void TearDown()
  {
    ros::spinOnce();

    scan.points.clear();
    dev->shutdown();
    delete dev;
    delete private_nh;
  }

  /** Callback function for the ts_scan topic.*/
  void store(const toposens_msgs::TsScan::ConstPtr& msg)
  {
    scan = *msg;
    std::cerr << TAG << "\033[33m" << "\tReceived scan of size "
      << scan.points.size() << "\033[00m\n";
  }
  
  /** Transmits given frame over the emulated sensor and polls the serial stream for data.*/
  void pollMockTx(std::string frame)
  {
    int n_bytes = frame.size();
    std::cerr << TAG << "\tTx nBytes: " << n_bytes << "\n";
    if (n_bytes < 100) std::cerr << TAG << "\tTx data: " << frame << "\n";
    std::cerr << TAG << "\tManually transmitting over: " << mock_sensor << "...";

    curr_frame = frame;
    write(open(mock_sensor.c_str(), O_RDWR | O_NOCTTY | O_NDELAY), frame.c_str(), n_bytes);
    std::cerr << "done\n";

    std::cerr << TAG << "\tSensor::poll over: " << driver_port << "...\n";
    dev->poll();
    ros::Duration(0.01).sleep();
    ros::spinOnce();
  }

  /** Sends artificial parameter update acknowledgements to unblock sensor constructor. */
  static void sendCommandAcknowledgements(){
    std::string mock_sensor;
    ros::NodeHandle* private_nh;
    private_nh = new ros::NodeHandle("~");
    private_nh->getParam("mock_sensor", mock_sensor);

    std::string acknowledgement("S000004C00003E");

    while(true){
      try
      {
        boost::this_thread::sleep(boost::posix_time::milliseconds(100));
        write(open(mock_sensor.c_str(), O_RDWR | O_NOCTTY | O_NDELAY),
              acknowledgement.c_str(),
              acknowledgement.size()
        );
        ros::spinOnce();
      }
      catch(boost::thread_interrupted&)
      {
        delete private_nh;
        return;
      }
    }
  }

  /** Tests attributes of point against expected x, y, z, v values. */
  void EXPECT_POINT(auto &pt, std::vector<float> exp, std::string caseMsg)
  {
    EXPECT_FLOAT_EQ(exp[0], pt.location.x) << "Failed for case: " << caseMsg;
    EXPECT_FLOAT_EQ(exp[1], pt.location.y) << "Failed for case: " << caseMsg;
    EXPECT_FLOAT_EQ(exp[2], pt.location.z) << "Failed for case: " << caseMsg;
    EXPECT_FLOAT_EQ(exp[3], pt.intensity)  << "Failed for case: " << caseMsg;
  }

  /** Records a failure if #scan contains points.*/
  void EXPECT_EMPTY(std::string caseMsg)
  {
    EXPECT_FILLED(caseMsg, 0);
  }

  /** Records a failure if #scan size or points within the scan deviate from expected output.*/
  void EXPECT_FILLED(std::string caseMsg, uint num_exp)
  {
    auto p = scan.points;
    if (p.size() == num_exp)
    {
      while (num_exp > 0) EXPECT_POINT(p[--num_exp], {-0.415, 0.01, 0.257, 0.61}, caseMsg);
    }
    else ADD_FAILURE() << "Invalid scan for case: " << caseMsg << "\r" << curr_frame;
    scan.points.clear();
  }
};

/** @test Tests Sensor::poll with a valid scan containing 4 points. */
TEST_F(SensorTest, pollValidFrame)
{
  std::cerr << TAG << "<pollValidFrame>\n";

  std::string frame = "S000016"
                        "P0000X-0415Y00010Z00257V00061"
                        "P0000X-0235Y00019Z00718V00055"
                        "P0000X-0507Y00043Z00727V00075"
                        "P0000X00142Y00360Z01555V00052"
                      "E";
  this->pollMockTx(frame);

  ASSERT_EQ(scan.points.size(), (uint)4) << "Not enough points in scan: " << scan.points.size();
  EXPECT_POINT(scan.points[0], {-0.415, 0.01,  0.257, 0.61}, "Valid point P1");
  EXPECT_POINT(scan.points[1], {-0.235, 0.019, 0.718, 0.55}, "Valid point P2");
  EXPECT_POINT(scan.points[2], {-0.507, 0.043, 0.727, 0.75}, "Valid point P3");
  EXPECT_POINT(scan.points[3], { 0.142, 0.360, 1.555, 0.52}, "Valid point P4");

  std::cerr << TAG << "</pollValidFrame>\n";
}

/** @test Polls empty or invalid data frames; expects zero points to be extracted.
 *  No error should be thrown in such cases, the frame should simply be ignored.
 *  An invalid empty data frame lacks either the frame header, or one of the characters 
 *  'S' or 'E', or at least one of the XYZV components.
 */
TEST_F(SensorTest, pollEmptyFrames)
{
  std::cerr << TAG << "<pollEmptyFrames>\n";

  this->pollMockTx("S000000E");
  EXPECT_EMPTY("Valid empty frame 1");

  this->pollMockTx("S153698E");
  EXPECT_EMPTY("Valid empty frame 2");

  this->pollMockTx("");
  EXPECT_EMPTY("Invalid empty frame 1");

  this->pollMockTx("SE");
  EXPECT_EMPTY("Invalid empty frame 2");

  this->pollMockTx("SXYZV");
  EXPECT_EMPTY("Invalid empty frame 3");

  std::cerr << TAG << "</pollEmptyFrames>\n";
}

/** @test Polls 1-point data frames for invalid data. A point is considered invalid when 
 *  any of its XYZV tags or values are missing. Checks that these frames result in zero extracted points. 
 *  No error should be thrown in such cases, the frame should simply be ignored.
 */
TEST_F(SensorTest, pollInvalidPoints)
{
  std::cerr << TAG << "<pollInvalidPoints>\n";

  this->pollMockTx("S000016P0000X-0415Y00010Z00257E");
  EXPECT_EMPTY("Missing intensity value");

  this->pollMockTx("S000016P0000X-0415Y00010Z00257V061E");
  EXPECT_EMPTY("Intensity with <5 bytes");

  this->pollMockTx("S000016P0000X-0415Y00010V00061E");
  EXPECT_EMPTY("Missing Z coordinate");

  this->pollMockTx("S000016P0000-0415Y00010Z00257V00061E");
  EXPECT_EMPTY("Missing X tag");

  this->pollMockTx("S000016P0000X-0415Y00010Y00257V00061E");
  EXPECT_EMPTY("Double Y coordinate");

  this->pollMockTx("S000016P0000-04150001000257V00061E");
  EXPECT_EMPTY("Missing XYZ tags");

  this->pollMockTx("S000016P0000X-04E5Y00010Z00257V00061E");
  EXPECT_EMPTY("Characters in X coordinate");

  std::cerr << TAG << "</pollInvalidPoints>\n";
}

/** @test Checks 2-point frames for invalid data. 
 *  A point is considered invalid when any of its XYZV tags or values are missing.
 *  Adjacent strings are auto concatenated by compiler.
 *  Only faulty data point(s) should be discarded, valid data points should be extracted into #scan.
 */
TEST_F(SensorTest, pollTwoMixedPoints)
{
  std::cerr << TAG << "<pollTwoMixedPoints>\n";

  this->pollMockTx("S000016"
                    "P0000X-0415Y00010Z00257V00061"
                    "P0000X-0235Y00019Y00718"
                   "E");
  EXPECT_FILLED("Double Y coordinate", 1);


  this->pollMockTx("S000016"
                    "P0000X-0415Y00010Z00257V00061"
                    "P0000X-0235Y00019Z00718"
                   "E");
  EXPECT_FILLED("Missing intensity value in P2", 1);


  this->pollMockTx("S000016"
                    "P0000X-0415Y00010Z00257V00061"
                    "P0000X-0235Y00019ZV00055"
                   "E");
  EXPECT_FILLED("Missing Z coordinate in P2", 1);


  this->pollMockTx("S000016"
                    "P0000-0235Y00019Z00718V00055"
                    "P0000-0415Y00010Z00257V00061"
                   "E");
  EXPECT_EMPTY("Missing X tag in both P1 and P2");


  this->pollMockTx("S000016"
                    "P0000X-0415Y00010Z00257V00061"
                    "PV00055"
                   "E");
  EXPECT_FILLED("XYZ tags missing in P2", 1);

  std::cerr << TAG << "</pollTwoMixedPoints>\n";
}

/** @test Checks 3-point frames for invalid data. 
 *  A point is considered invalid when any of its XYZV tags or values are missing.
 *  Adjacent strings are auto concatenated by compiler.
 *  Only faulty data point(s) should be discarded, valid data points should be extracted into #scan.
 */
TEST_F(SensorTest, pollThreeMixedPoints)
{
  std::cerr << TAG << "<pollThreeMixedPoints>\n";

  this->pollMockTx("S000016"
                    "P0000X-0415Y00010Z00257V00061"
                    "P0000X-0415Y00010Z00257V00061"
                    "P0000X-0235Y00019Z0071800055"
                   "E");
  EXPECT_FILLED("Missing intensity tag in P3", 2);


  this->pollMockTx("S000016"
                    "P0000X-0235Y00019Y00718V00055"
                    "P0000X-0415Y00010Z00257V00061"
                    "P0000X-0415Y00010Z00257Y00061"
                   "E");
  EXPECT_FILLED("Double Y coordinate in P1 and P3", 1);


  this->pollMockTx("S000016"
                    "P0000X-0415Y00010Z00257V00061"
                    "P0000X-0Z35Y00019Z00718V00055"
                    "P0000X-0415Y00010Z00257V00061"
                   "E");
  EXPECT_FILLED("Non-numerical characters in X coordinate of P2", 2);

  std::cerr << TAG << "</pollThreeMixedPoints>\n";
}

/** @test Polls a large data frame of 500 points.
 *  Expects the polling to be completed without any exception thrown.
 *  Some points will inevitably get dropped due to bad serial stream
 *  characters but they will always be less than equal to tx points, hence
 *  the function conducts a greater-than check.
 */
TEST_F(SensorTest, pollLargeFrame)
{
  std::cerr << TAG << "<pollLargeFrame>\n";

  uint numPoints = 500;
  std::string frame = "S000016";
  while (numPoints--) frame += "P0000X-0415Y00010Z00257V00061";
  frame += "E";
  EXPECT_NO_THROW(this->pollMockTx(frame));

  // @todo confirm if EXPECT_GT is working and try to get more points: ~500
  // EXPECT_LE(scan.points.size(), numPoints);
  EXPECT_GT(scan.points.size(), (long unsigned int)350);

  std::cerr << TAG << "</pollLargeFrame>\n";
}

/** @test Polls valid data frames from a dump file.
 *  Expects the polling to be completed without any exception thrown.
 */
TEST_F(SensorTest, pollStreamDump)
{
  std::cerr << TAG << "<pollStreamDump>\n";

  std::string filename;
  private_nh->getParam("file", filename);

  std::ifstream infile;
  infile.open(filename);

  std::string line;
  while (std::getline(infile, line, 'S'))
  {
    if (line.empty()) continue;
    EXPECT_NO_THROW(this->pollMockTx("S" + line));
  }
  infile.close();

  std::cerr << TAG << "</pollStreamDump>\n";
}

/** @test Shuts down the emulated port and expects no exception to be thrown when
 *  a new connection is re-attempted on the same port.
 */
TEST_F(SensorTest, devShutdown)
{
  std::cerr << TAG << "<devShutdown>\n";
  std::cerr << TAG << "\tShutting down device on " << driver_port << "...";

  ros::NodeHandle nh;
  dev->shutdown();
  std::cerr << "done\n";

  boost::thread ackThread(sendCommandAcknowledgements);
  EXPECT_NO_THROW(std::make_unique<Sensor>(nh, *private_nh));
  ackThread.interrupt(); // Send interrupt to thread
  ackThread.join(); // Hold main process until thread terminates.

  std::cerr << TAG << "</devShutdown>\n";
}


int main(int argc, char **argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "ts_driver_sensor_test");
  ros::NodeHandle nh;
  return RUN_ALL_TESTS();
}
