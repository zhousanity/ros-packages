find_package(roslaunch REQUIRED)
find_package(rostest REQUIRED)

add_definitions(
	-Wall
	-O2
	-D_GNU_SOURCE
	-Wno-unused-but-set-variable
	-Wno-unused-result
	-Wno-conversion-null
)


#############
## Modules ##
#############

set(TEST_MODULES
	sensor_manager
)

foreach(M ${TEST_MODULES})

	add_rostest_gtest(
		${PROJECT_NAME}_${M}_test
		modules/${M}/launch.test
		modules/${M}/tests.cpp
	)

	target_link_libraries(
		${PROJECT_NAME}_${M}_test
		${PROJECT_NAME}
	)
endforeach()


###################
## Code Coverage ##
###################
#include(code_coverage.cmake)
#coverage_add_target(run_tests_${PROJECT_NAME})
