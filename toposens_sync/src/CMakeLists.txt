#############
## Library ##
#############

add_library(
  ${PROJECT_NAME}
  lib/sensor_manager.cpp
)

target_link_libraries(
  ${PROJECT_NAME}
  ${catkin_LIBRARIES}
)

install(
  TARGETS ${PROJECT_NAME}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
)


################
## Executable ##
################

add_executable(
  ${PROJECT_NAME}_node
  ts_sync_node.cpp
)

target_link_libraries(
  ${PROJECT_NAME}_node
  ${PROJECT_NAME}
)

install(
  TARGETS ${PROJECT_NAME}_node
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
  COMPONENT main
)
