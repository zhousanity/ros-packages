[![Toposens](https://toposens.com/wp-content/themes/toposens/assets/img/logo2.png)](https://toposens.com)


# Overview

ROS packages for working with Toposens 3D Ultrasound sensors: http://toposens.com/products/

Developed and tested for [ROS Melodic](http://wiki.ros.org/melodic) on [Ubuntu 18.04 (Bionic)](http://releases.ubuntu.com/18.04/)


# 1. Setup

### For Ubuntu 18.04

* Follow these guides to install and set up ROS Melodic on your system:

    * http://wiki.ros.org/melodic/Installation/Ubuntu
    * http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment

* Install catkin tools:

    `sudo apt install python-catkin-tools`

### For Ubuntu 16.04

* Follow these guides to install and set up ROS Kinetic on your system:

    * http://wiki.ros.org/kinetic/Installation/Ubuntu
    * http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment

* Install catkin tools:

    `sudo apt install python-catkin-tools`


# 2. Build Driver

* Clone the toposens metapackage into your catkin workspace:

    `cd ~/catkin_ws/src`

    `git clone https://gitlab.com/toposens/public/ros-packages.git`

* Use rosdep to install missing dependencies:

    `rosdep update`

    `rosdep install --from-paths ~/catkin_ws/src/ros-packages/ --ignore-src --rosdistro <distro> -y`


* Build the package by using catkin build:

    `catkin build toposens`


# 3. Enable Serial Port Permissions

* Add yourself to the “dialout” group

    `sudo adduser $USER dialout`

* Trigger updated group permissions to take effect

    `newgrp dialout`

* Re-login or reboot for the updated group permissions to take effect permanently.


# 4. Connect TS3 Sensor

* Connect your Toposens TS3 sensor via the provided FTDI cable to any available USB port on your computer.

* Obtain the connected terminal ID for the device

    `dmesg | grep "FTDI USB Serial Device"`

* In the case below, the terminal ID would be `ttyUSB0`

<img src="http://85.214.248.206/guide/Port.png" width="700">


# 5. Visualize Data

There are three different ways of viewing the data output from a TS3 sensor, which are published to three separate topics:
* `/ts_scans` broadcasts messages of type [toposens_msgs/TsScan](http://docs.ros.org/api/toposens_msgs/html/msg/TsScan.html), which hold the raw data stream parsed as `toposens_msgs`
* `/ts_markers` broadcasts messages of type [visualization_msgs/MarkerArray](http://docs.ros.org/api/visualization_msgs/html/msg/MarkerArray.html) for simple visualization with RViz
* `/ts_clouds` broadcasts messages of type [sensor_msgs/PointCloud2](http://docs.ros.org/api/sensor_msgs/html/msg/PointCloud2.html), which can be visualized in RViz and also logged to a file

The utility of these topics is further explored in the following sections.


### View Raw Stream

The `ts_driver_node` translates the sensor data into custom [toposens_msgs/TsScan](http://docs.ros.org/api/toposens_msgs/html/msg/TsScan.html) messages.

* Launch the driver node and start accruing data from a TS3 sensor. Set the corresponding serial port as launch argument (here: /dev/ttyUSB0):

    `roslaunch toposens_driver toposens_driver.launch port:=/dev/ttyUSB0`

* The sensor data is published to the the topic `/ts_scans`

    `rostopic echo /ts_scans`

<img src="http://85.214.248.206/guide/TsScan.png" width="700">

---
### Visualize Data as Markers

The `ts_markers_node` translates the messages of type [toposens_msgs/TsScan](http://docs.ros.org/api/toposens_msgs/html/msg/TsScan.html) into messages of type [visualization_msgs/MarkerArray](http://docs.ros.org/api/visualization_msgs/html/msg/MarkerArray.html), which can be visualized in RViz.

* The driver node as well as the markers node are launched from within one launch-file:

    `roslaunch toposens_markers toposens_markers.launch port:=/dev/ttyUSB0`

The markers are visualized in RViz:

<img src="http://85.214.248.206/guide/RVizMarkers.png" width="700">

---
### View Pointcloud in RViz

The `ts_cloud_node` translates the messages of type [toposens_msgs/TsScan](http://docs.ros.org/api/toposens_msgs/html/msg/TsScan.html) into messages of type [sensor_msgs/PointCloud2](http://docs.ros.org/api/sensor_msgs/html/msg/PointCloud2.html).

* The driver node as well as the pointcloud node are launched from within one launch-file:

    `roslaunch toposens_pointcloud toposens_cloud.launch port:=/dev/ttyUSB0`

The pointcloud is visualized in RViz:

<img src="http://85.214.248.206/guide/RVizPointcloud.png" width="700">


# 6. Manipulate Parameters

* To manipulate sensor parameters as well as visualization parameters live in realtime, run in an additional terminal window:

    `rosrun rqt_reconfigure rqt_reconfigure`

    The `ts_driver_node` respectively `ts_markers_node` need to be running for this to work.

### Sensor Parameters

There are 6 different parameters that can be changed in the `ts_driver_node`.

<img src="http://85.214.248.206/guide/DynRecDriver.png" width="600">

| Variable Name             | Effect  |
| ------------------------- | ------- |
| num_pulse                 | Number of ultrasonic pulses emitted by the sensors piezo transducer in every transmission cycle. Increasing the value will detect objects that are further away, decreasing it will increase the precision in short range. |
| peak_detection_window     | Kernel size applied on ADC signals for peak detection. Decreasing it will separate multiple objects that are close to each other. |
| echo_rejection_threshold  | Minimum amplitude for an echo to be considered valid. Increasing it will ignore more points of smaller intensity. |
| use_external_temperature  | If checked uses external temperature value to calibrate speed-of-sound, otherwise use value provided by on-board temperature sensor. |
| external_temperature      | Temperature value used to calibrate speed-of-sound. Value is only used if use_external_temperature is checked. |
| noise_indicator_threshold | Normalized noise level on ADC signals to mark processed points as noisy. |

### Marker Parameters

There are 2 parameters that can be changed in the `ts_markers_node`.

<img src="http://85.214.248.206/guide/DynRecMarkers.png" width="600">

| Variable Name | Effect  |
| ------------- | ------- |
| lifetime      | Duration for which a marker should remain visible |           
| scale         | Magnitude for resizing markers equally and simultaneously |


# 7. Turtlebot Integration

Basic mapping of the environment with [sensor_msgs/PointCloud2](http://docs.ros.org/api/sensor_msgs/html/msg/PointCloud2.html) can be done by orchestrating a TurtelBot3 Burger with a TS3 ultrasonic sensor.

### Setup

It is assumed that the TurtleBot has already been setup, its onboard computer (Raspberry Pi) is running e.g. [ROS Melodic](http://wiki.ros.org/melodic) on [Ubuntu Mate 18.04](https://ubuntu-mate.org/) and its IP address is known (`<turtlebot_ip>`, e.g. 192.168.0.179).

* In order to access the TurtelBot's onboard computer, use SSH:

    `ssh pi@<turtlebot_ip>`

* **[TurtelBot]** Clone the TurtleBot repositories into your catkin workspace:

    `git clone https://github.com/ROBOTIS-GIT/turtlebot3.git`

    `git clone https://github.com/ROBOTIS-GIT/turtlebot3_msgs.git`

    `git clone https://github.com/ROBOTIS-GIT/hls_lfcd_lds_driver.git`

    (The latter repository is only needed if the TurtleBot's Laser Distance Sensor shall be used.)

* **[TurtelBot]** Use rosdep to install missing dependencies

   `rosdep update`

   `rosdep install --from-paths ~/catkin_ws/src/ros-packages/ --ignore-src --rosdistro <distro> -y`

* **[TurtelBot]** Build the packages by making use of catkin_make:

    `cd ~/catkin_ws`

    `catkin_make`

    or catkin build:

    `catkin build turtlebot3_msgs turtlebot3`

* **[TurtelBot]** Follow sections 2. to 4. of this guide to setup the Toposens TS3 ultrasonic 3D sensor on the TurtelBot. The sensor can be mounted on a TurtleBot3 Burger via the mounting device, as can be seen in the picture below.

<img src="http://85.214.248.206/guide/TurtleBotTS3.png" width="600">

* **[Remote PC]** Get to know your own IP address (`<master_ip>`, e.g. 192.168.0.118):

    `ifconfig | grep inet`

* **[Remote PC]** Add necessary parameters to your machine's bashrc (`~/.bashrc`):

    ```
    export TURTLEBOT3_MODEL='burger'
    export ROS_MASTER_URI=http://<master_ip>:11311
    export ROS_HOSTNAME=<master_ip>
    ```

    Source the bashrc:

    `source ~/.bashrc`

* **[TurtelBot]** Add necessary parameters in the TurtleBot's bashrc (`~/.bashrc`):

    ```
    export TURTLEBOT3_MODEL='burger'
    export ROS_MASTER_URI=http://<master_ip>:11311
    export ROS_HOSTNAME=<turtlebot_ip>
    ```

    Source the bashrc:

    `source ~/.bashrc`

### Mapping

* **[Remote PC]** Launch the pointcloud node:

    `roslaunch toposens_pointcloud turtlebot_cloud.launch`

* **[TurtleBot]** Launch the driver node:

    `roslaunch toposens_driver toposens_turtlebot.launch`

* **[Remote PC]** The TurtleBot can be controlled via the keyboard. For an instruction see the [TurtleBot Documentation](http://emanual.robotis.com/docs/en/platform/turtlebot3/teleoperation/#teleoperation). It is not required to launch the launchfile for teleoperation. This is already done by the command above.

* When navigating through a room, the robot is accumulating the TS3 sensor data and mapping the environment via [sensor_msgs/PointCloud2](http://docs.ros.org/api/sensor_msgs/html/msg/PointCloud2.html). Make sure to set the "Decay Time" of the pointcloud large enogh in RViz.

<img src="http://85.214.248.206/guide/TurtleBotMappingPhoto.JPG" width="500">

<img src="http://85.214.248.206/guide/TurtleBotMappingRViz.png" width="700">


# Code API
See the Doxygen documentation for the
[toposens_msgs](http://docs.ros.org/melodic/api/toposens_msgs/html/index-msg.html),
[toposens_driver](http://docs.ros.org/melodic/api/toposens_driver/html/toposens_driver/annotated.html),
[toposens_markers](http://docs.ros.org/melodic/api/toposens_markers/html/toposens_markers/annotated.html),
[toposens_pointcloud](http://docs.ros.org/melodic/api/toposens_pointcloud/html/toposens_pointcloud/files.html), and
[toposens_sync](http://docs.ros.org/melodic/api/toposens_sync/html/annotated.html) packages respectively.
